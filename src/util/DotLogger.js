/* eslint-disable no-underscore-dangle */
const { Transform } = require('stream');
const { dotRate } = require('../config');

let globalTransferred = 0; // Amount of transferred bytes using DotWriter (Global for app)

class DotLogger extends Transform {
  _transform(chunk, encoding, next) {
    globalTransferred += chunk.length;

    if (globalTransferred >= dotRate) {
      const dotAmount = Math.floor(globalTransferred / dotRate);
      globalTransferred %= dotRate;

      process.stdout.write('.'.repeat(dotAmount));
    }

    this.push(chunk);
    next();
  }
}

module.exports = DotLogger;
