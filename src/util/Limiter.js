/* eslint-disable no-underscore-dangle */
const { Transform } = require('stream');

const second = 1000; // 1000 ms

class Limiter extends Transform {
  constructor(rate) {
    super();
    this.rate = rate;
  }

  _transform(chunk, encoding, next) {
    const delay = second / (this.rate / chunk.length);

    setTimeout(() => {
      this.push(chunk);
      next();
    }, delay);
  }
}

module.exports = Limiter;
