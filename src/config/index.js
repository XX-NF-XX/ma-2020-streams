module.exports = {
  port: process.env.PORT || 3000,
  filePath: './static/image.jpg',
  rate: 1024 * 1024,
  minRate: 128 * 1024,
  dotRate: 1024 * 1024, // one dot per N bytes
};
