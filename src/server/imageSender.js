const fs = require('fs');
const { pipeline } = require('stream');

const { endResponse } = require('./helpers');
const Limiter = require('../util/Limiter');
const DotLogger = require('../util/DotLogger');
const config = require('../config');

function sendJPEG(response) {
  const imageReader = fs.createReadStream(config.filePath);
  imageReader.on('error', err => {
    console.error(`Image read stream failed while reading ${config.filePath}!`, err.stack);
    endResponse(response, 500);
  });

  const rate = Math.max(config.rate, config.minRate);
  const limiter = new Limiter(rate);
  limiter.on('error', err => {
    console.error(`Limiter failed!`, err.stack);
    endResponse(response, 500);
  });

  const dotLogger = new DotLogger();
  dotLogger.on('error', err => {
    console.error(`Dot Logger failed!`, err.stack);
    endResponse(response, 500);
  });

  pipeline(imageReader, limiter, dotLogger, response, err => {
    if (err) {
      console.error(`JPEG sending pipeline failed!`, err.message);
    }
  });
}

module.exports = {
  sendJPEG,
};
